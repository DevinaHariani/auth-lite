<?php

class Hash {
    public static function make(string $plainText){
        return password_hash($plainText, PASSWORD_BCRYPT); //hashing
    }

    public static function verify(string $plainText, string $hash){
        return password_verify($plainText, $hash);
    }
}